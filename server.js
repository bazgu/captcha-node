function addChallenge (token, value, time) {
    challenges[token] = Challenge(value, time, () => {
        Log.info('Challenge ' + token + ' solved')
        delete challenges[token]
    }, () => {
        Log.info('Challenge ' + token + ' expired')
        delete challenges[token]
    })
}

function shutdown () {
    for (var i in challenges) {
        var content = JSON.stringify(challenges[i].toStorageObject())
        fs.writeFileSync('dump/' + Buffer.from(i).toString('hex'), content)
    }
    process.exit()
}

process.chdir(__dirname)

var version = process.argv[2]

var fs = require('fs'),
    url = require('url')

var Challenge = require('./lib/Challenge.js'),
    Error404Page = require('./lib/Error404Page.js'),
    Log = require('./lib/Log.js'),
    StaticPage = require('./lib/StaticPage.js')

var challenges = Object.create(null)
fs.readdirSync('dump').forEach(filename => {
    if (filename === '.gitignore') return
    var token = Buffer.from(filename, 'hex').toString('utf8')
    var path = 'dump/' + filename
    var object = JSON.parse(fs.readFileSync(path, 'utf8'))
    addChallenge(token, object.value, object.time)
    fs.unlinkSync(path)
})

var issue = require('./lib/Issue.js')(addChallenge)

var pages = Object.create(null)
pages['/'] = require('./lib/IndexPage.js')
pages['/accountNode/verify'] = require('./lib/VerifyPage.js')(issue, challenges)
pages['/frontNode/get'] = require('./lib/GetPage.js')(issue)
pages['/index.css'] = StaticPage('files/index.css', 'text/css')
pages['/index.js'] = StaticPage('files/index.js', 'application/javascript')
pages['/node'] = require('./lib/NodePage.js')(version)

require('./lib/Server.js')((req, res) => {
    Log.http(req.method + ' ' + req.url)
    var parsedUrl = url.parse(req.url, true)
    var page = pages[parsedUrl.pathname]
    if (page === undefined) page = Error404Page
    page(req, res, parsedUrl)
})

process.on('SIGINT', shutdown)
process.on('SIGTERM', shutdown)
