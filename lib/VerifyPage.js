module.exports = (issue, challenges) => request => {

    const query = request.parsed_url.query

    const challenge = challenges[query.token]
    if (challenge === undefined) {
        request.respond({
            error: 'INVALID_TOKEN',
            newCaptcha: issue(query.prefix),
        })
        return
    }

    if (challenge.value !== query.value) {
        request.respond('INVALID_VALUE')
        return
    }

    challenge.solve()
    request.respond(true)

}
