module.exports = (issue, challenges) => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var challenge = challenges[query.token]
        if (challenge === undefined) {
            res.end(JSON.stringify({
                error: 'INVALID_TOKEN',
                newCaptcha: issue(query.prefix),
            }))
            return
        }

        if (challenge.value !== query.value) {
            res.end('"INVALID_VALUE"')
            return
        }

        challenge.solve()
        res.end('true')

    }
}
