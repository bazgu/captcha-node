module.exports = (value, time, solveListener, expireListener) => {
    var timeout = setTimeout(expireListener, time - Date.now() + 1000 * 60 * 5)
    return {
        time: time,
        value: value,
        solve: () => {
            clearTimeout(timeout)
            solveListener()
        },
        toStorageObject: () => {
            return {
                time: time,
                value: value,
            }
        },
    }
}
