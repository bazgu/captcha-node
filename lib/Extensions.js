const map = Object.create(null)
map['.css'] = 'text/css'
map['.ico'] = 'image/x-icon'
map['.jpg'] = 'image/jpeg'
map['.js'] = 'application/javascript'
map['.png'] = 'image/png'
map['.svg'] = 'image/svg+xml'
map['.wav'] = 'audio/wav'

module.exports = map
