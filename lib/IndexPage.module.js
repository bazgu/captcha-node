module.exports = app => request => {
    app.EchoText(request, {
        type: 'text/html; charset=UTF-8',
        content:
            '<!DOCTYPE html>' +
            '<html>' +
                '<head>' +
                    '<title>Captcha Node</title>' +
                    '<meta charset="UTF-8" />' +
                    '<link rel="stylesheet" href="index.css" />' +
                '</head>' +
                '<body>' +
                    '<h1>Captcha Node</h1>' +
                    '<fieldset>' +
                        '<legend>get</legend>' +
                        '<form id="getForm">' +
                            '<img id="image" />' +
                            '<div class="next">' +
                                'prefix:<br />' +
                                '<input class="withButton" id="prefixInput" value="samplePrefix" />' +
                                '<button>get</button>' +
                            '</div>' +
                        '</form>' +
                    '</fieldset>' +
                    '<fieldset>' +
                        '<legend>verify</legend>' +
                        '<form id="verifyForm">' +
                            'token:<br />' +
                            '<input id="tokenInput" />' +
                            '<div class="next">' +
                                'value:<br />' +
                                '<input class="withButton" id="valueInput" />' +
                                '<button>verify</button>' +
                            '</div>' +
                            '<div class="next" style="display: none" id="statusElement"></div>' +
                        '</form>' +
                    '</fieldset>' +
                    '<script src="index.js"></script>' +
                '</body>' +
            '</html>',
    })
}
