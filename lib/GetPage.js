module.exports = issue => request => {
    request.respond(issue(request.parsed_url.query.prefix))
}
