module.exports = issue => {
    return (req, res, parsedUrl) => {
        res.setHeader('Content-Type', 'application/json')
        res.end(JSON.stringify(issue(parsedUrl.query.prefix)))
    }
}
