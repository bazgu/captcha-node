const crypto = require('crypto')
const fs = require('fs')
const path = require('path')
const zlib = require('zlib')

module.exports = app => filename => {

    let content_type = app.Extensions[path.extname(filename)]
    if (content_type === undefined) content_type = 'application/octet-stream'

    const content = fs.readFileSync(filename)
    const etag = crypto.createHash('md5').update(content).digest('hex')
    const gzip_content = zlib.gzipSync(content)
    const gzip_etag = etag + '-gzip'

    return request => {

        const res = request.res
        res.setHeader('Content-Type', content_type)
        res.setHeader('Cache-Control', 'public, max-age=31536000')

        ;(plain => {

            if (gzip_content.length >= content.length) {
                plain()
                return
            }

            res.setHeader('Vary', 'Accept-Encoding')

            if (app.AcceptGzip(request.req)) {
                res.setHeader('Content-Encoding', 'gzip')
                res.setHeader('ETag', gzip_etag)
                res.end(gzip_content)
                return
            }

            plain()

        })(() => {
            res.setHeader('ETag', etag)
            res.end(content)
        })

    }

}
