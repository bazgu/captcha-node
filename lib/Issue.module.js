module.exports = app => addChallenge => {

    const canvasWidth = 140,
        canvasHeight = 44,
        padding = 10,
        numChars = 5,
        canvas = require('canvas').createCanvas(canvasWidth, canvasHeight),
        spaceX = (canvasWidth - padding * 2) / numChars,
        charX = padding + spaceX * 0.5,
        charY = canvasHeight / 2,
        charset = '23456789abcdefghijkmnpqrstuvwxyz'

    const fonts = []
    for (let i = 0; i < 15; i++) fonts.push('normal ' + (i + 19) + 'px serif')

    const c = canvas.getContext('2d')
    c.font = 'normal 20px serif'
    c.textBaseline = 'middle'
    c.textAlign = 'center'

    return prefix => {

        c.fillStyle = 'hsl(0, 0%, 95%)'
        c.fillRect(0, 0, canvasWidth, canvasHeight)

        let value = ''
        c.fillStyle = 'black'
        c.save()
        c.translate(charX, charY)
        for (let i = 0; i < numChars; i++) {

            const ch = charset[Math.floor(Math.random() * charset.length)]
            value += ch

            c.save()
            c.font = fonts[Math.floor(Math.random() * fonts.length)]
            c.translate(Math.random() * 20 - 10, Math.random() * 16 - 8)
            c.rotate((Math.random() - 0.5) * Math.PI * 0.4)
            c.fillText(ch, 0, 0)
            c.restore()

            c.translate(spaceX, 0)

        }
        c.restore()

        const token = prefix + '_' + app.RandomBytes(20).toString('hex')
        addChallenge(token, value, Date.now())

        return {
            token,
            image: canvas.toDataURL(),
        }

    }

}
