module.exports = (req, res) => {
    res.setHeader('Content-Type', 'text/html; charset=UTF-8')
    res.end(
        '<!DOCTYPE html>' +
        '<html>' +
            '<head>' +
                '<title>Captcha Node</title>' +
                '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
                '<link rel="stylesheet" type="text/css" href="index.css" />' +
            '</head>' +
            '<body>' +
                '<h1>Captcha Node</h1>' +
                '<fieldset>' +
                    '<legend>get</legend>' +
                    '<form id="getForm">' +
                        '<img id="image" />' +
                        '<div class="next">' +
                            'prefix:<br />' +
                            '<input class="withButton" type="text" id="prefixInput" value="samplePrefix" />' +
                            '<button>get</button>' +
                        '</div>' +
                    '</form>' +
                '</fieldset>' +
                '<fieldset>' +
                    '<legend>verify</legend>' +
                    '<form id="verifyForm">' +
                        'token:<br />' +
                        '<input type="text" id="tokenInput" />' +
                        '<div class="next">' +
                            'value:<br />' +
                            '<input class="withButton" type="text" id="valueInput" />' +
                            '<button>verify</button>' +
                        '</div>' +
                        '<div class="next" style="display: none" id="statusElement"></div>' +
                    '</form>' +
                '</fieldset>' +
                '<script type="text/javascript" src="index.js"></script>' +
            '</body>' +
        '</html>'
    )
}
