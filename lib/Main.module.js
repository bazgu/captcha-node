const fs = require('fs')
const url = require('url')

module.exports = app => () => {

    function addChallenge (token, value, time) {
        ;(log => {
            challenges[token] = app.Challenge(value, time, () => {
                log.info('Solved')
                delete challenges[token]
            }, () => {
                log.info('Expired')
                delete challenges[token]
            })
            log.info('Issued')
        })(log.sublog('Chellenge ' + token))
    }

    function shutdown () {
        for (const i in challenges) {
            const content = JSON.stringify(challenges[i].toStorageObject())
            fs.writeFileSync('dump/' + Buffer.from(i).toString('hex'), content)
        }
        process.exit()
    }

    const log = app.log

    const listen = app.config.listen

    const challenges = Object.create(null)
    fs.readdirSync('dump').forEach(filename => {
        if (filename === '.gitignore') return
        const token = Buffer.from(filename, 'hex').toString('utf8')
        const path = 'dump/' + filename
        const object = JSON.parse(fs.readFileSync(path, 'utf8'))
        addChallenge(token, object.value, object.time)
        fs.unlinkSync(path)
    })

    const issue = app.Issue(addChallenge)

    const pages = Object.create(null)
    pages['/'] = app.IndexPage
    pages['/accountNode/verify'] = app.VerifyPage(issue, challenges)
    pages['/frontNode/get'] = app.GetPage(issue)
    app.Static(pages)

    require('http').createServer((req, res) => {

        log.info('Http ' + req.method + ' ' + req.url)

        const parsed_url = url.parse(req.url, true)
        const request = {
            parsed_url, req, res,
            respond (response) {
                app.EchoText(request, {
                    type: 'application/json',
                    content: JSON.stringify(response),
                })
            },
        }

        const page = (() => {
            const page = pages[parsed_url.pathname]
            if (page !== undefined) return page
            return app.Error404Page
        })()

        page(request)

    }).listen(listen.port, listen.host)

    process.on('SIGINT', shutdown)
    process.on('SIGTERM', shutdown)

    app.Watch(['lib', 'static'])

    log.info('Started')
    log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}
