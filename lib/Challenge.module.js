module.exports = app => (value, time, solveListener, expireListener) => {
    const abort = app.Timeout(expireListener, time - Date.now() + 1000 * 60 * 5)
    return {
        time, value,
        solve () {
            abort()
            solveListener()
        },
        toStorageObject () {
            return { time, value }
        },
    }
}
