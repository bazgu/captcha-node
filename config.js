module.exports = {
    debug_mode: true,
    listen: {
        port: 7200,
        host: '127.0.0.1',
    },
}
