function load () {
    const request = new XMLHttpRequest
    request.open('get', 'frontNode/get?prefix=' + encodeURIComponent(prefixInput.value))
    request.send()
    request.onload = () => {
        setCaptcha(JSON.parse(request.responseText))
    }
}

function setCaptcha (captchaObject) {
    tokenInput.value = captchaObject.token
    image.src = captchaObject.image
}

const image = document.getElementById('image'),
    prefixInput = document.getElementById('prefixInput'),
    tokenInput = document.getElementById('tokenInput'),
    statusElement = document.getElementById('statusElement'),
    valueInput = document.getElementById('valueInput')

const getForm = document.getElementById('getForm')
getForm.addEventListener('submit', e => {
    e.preventDefault()
    load()
})

const verifyForm = document.getElementById('verifyForm')
verifyForm.addEventListener('submit', e => {

    e.preventDefault()

    const url = 'accountNode/verify' +
        '?token=' + encodeURIComponent(tokenInput.value) +
        '&value=' + encodeURIComponent(valueInput.value) +
        '&prefix=' + encodeURIComponent(prefixInput.value)

    const request = new XMLHttpRequest
    request.open('get', url)
    request.send()
    request.onload = () => {
        const response = JSON.parse(request.responseText)
        statusElement.style.display = 'block'
        if (response.error === 'INVALID_TOKEN') {
            setCaptcha(response.newCaptcha)
            statusElement.innerHTML = response.error
        } else {
            statusElement.innerHTML = response
        }
    }

})

load()
