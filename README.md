Bazgu Captcha Node
==================

This program **captcha-node** is one of the nodes in a Bazgu infrastructure.
A captcha-node serves for issuing CAPTCHA challenges for users to solve.

Workflow
--------

A front-node obtains a new challenge from a captcha-node.
A user solves the challenge and sends it to the account-node
along with other details required for creating an account.
The account-node then checks if the solution is correct.
If it's incorrect a new challenge is issued by the captcha-node.
Every challenge expires in five minutes after issuing.

Configuration
-------------

See `config.js` for details.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
