function load () {
    var request = new XMLHttpRequest
    request.open('get', 'frontNode/get?prefix=' + encodeURIComponent(prefixInput.value))
    request.send()
    request.onload = function () {
        setCaptcha(JSON.parse(request.responseText))
    }
}

function setCaptcha (captchaObject) {
    tokenInput.value = captchaObject.token
    image.src = captchaObject.image
}

var image = document.getElementById('image'),
    prefixInput = document.getElementById('prefixInput')
    tokenInput = document.getElementById('tokenInput'),
    statusElement = document.getElementById('statusElement'),
    valueInput = document.getElementById('valueInput')

var getForm = document.getElementById('getForm')
getForm.addEventListener('submit', function (e) {
    e.preventDefault()
    load()
})

var verifyForm = document.getElementById('verifyForm')
verifyForm.addEventListener('submit', function (e) {

    e.preventDefault()

    var url = 'accountNode/verify' +
        '?token=' + encodeURIComponent(tokenInput.value) +
        '&value=' + encodeURIComponent(valueInput.value) +
        '&prefix=' + encodeURIComponent(prefixInput.value)

    var request = new XMLHttpRequest
    request.open('get', url)
    request.send()
    request.onload = function () {
        var response = JSON.parse(request.responseText)
        statusElement.style.display = 'block'
        if (response.error === 'INVALID_TOKEN') {
            setCaptcha(response.newCaptcha)
            statusElement.innerHTML = response.error
        } else {
            statusElement.innerHTML = response
        }
    }

})

load()
